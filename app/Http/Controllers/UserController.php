<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\userRequest;
use App\User;

class UserController extends Controller
{
    public function show($id){
    	$showuser = User::findOrfail($id);
    	return $showuser;         
        
    }

    public function home(){
    	return view('home');
    }

    public function getUser(){
        $user = Auth::user();
        return $user;
    }

    public function store(Request $request)
    {   
        $data = $request->all();
        $userdata['id'] = $data['tel'];
        $userdata['name'] = $data['name'];
        $user = new User($data);
        $user->save();
        return redirect('home');
    }
}
